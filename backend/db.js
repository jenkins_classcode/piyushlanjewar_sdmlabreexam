const mysql = require('mysql2')

const createDatabaseConnection = ()=>{
    const connection = mysql.createConnection({
        host:'db',   //db
        port:3306,
        user:'root',
        password:'root',  //change it to root
        database:'mydb', //same as dockerfile of DB
    })

    connection.connect()

    return connection
}

module.exports={
    createDatabaseConnection
}